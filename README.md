# Hull Admin Components

## Usage

Configuration : copy config.example.yml to config.yml and replace the content with your Hull app's settings.

## Components

To run the component demo :

Install grunt and dependencies

    npm install -g grunt-cli
    npm install

Start the default grunt task

    grunt
    open http://localhost:8000


### facets

    <div data-hull-component="facets" data-hull-terms="app_names,main_identity"></div>


## Example Queries with PHP Client

check the [php](/php/index.php) example file in this repo.

Install composer and dependencies

	cd php
	composer install
	php -S localhost:9000
	open http://localhost:9000/index.php?facets=main_identity,app_names

## Going further with Elasticsearch APIs

Elasticsearch documentation on Facets:

http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/search-facets.html

Elasticsearch documentation for Filters:

http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/query-dsl-filters.html
