<?php
require_once('config.php');

if (isset($_GET['facets'])) {
	$facets_names = split(',', $_GET['facets']);
} else {
	$facets_names = array('app_names');
}

$facets = array();
foreach ($facets_names as $facet_name) {
	$facets[$facet_name] = array('terms' => array('field' => ($facet_name . '.exact')));
}


$result = $hull->post('search/user_reports', array('limit' => 1, 'facets' => $facets));

?>

<html>
<head>
  <title>Hull Users Facets</title>
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
  <script src="//hull-js.s3.amazonaws.com/<?php echo $config['version'] ?>/hull.debug.js"></script>
  <script>
  	Hull.init(<?php echo json_encode($config['init']); ?>);
  </script>
</head>
<body>
	<div class="container">
		<div class="row">
		<?php foreach ($result->facets as $facetName => $facet): ?>
		<div class="col-md-4">
			<div class="panel panel-default">
			  <div class="panel-heading text-center">
			    <h3 class="panel-title">
						<?php echo $facetName; ?>
			    </h3>
			  </div>
		    <table class="table">
		    	<?php foreach ($facet->terms as $term): ?>
		    	<tr>
		    		<th><?php echo $term->count ?></th>
		    		<td><?php echo $term->term ?></td>
		    	</tr>
		    	<? endforeach ?>
		    </table>
		    <div class="panel-footer text-center">
		    	<b><?php echo $facet->total ?></b> Total
					/
		    	<b><?php echo $facet->missing ?></b> Missing
		    	/
		    	<b><?php echo $facet->other ?></b> Other
		    </div>
			</div>
		</div>
		<? endforeach ?>
		</div>
	</div>
</body>
</html>
