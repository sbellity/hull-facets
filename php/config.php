<?php
require 'vendor/autoload.php';
use Symfony\Component\Yaml\Yaml;
$config = Yaml::parse(file_get_contents('../config.yml'));

$hull = new Hull_Client(array( 'hull' => array(
  'host' => split('//', $config['init']['orgUrl'], 2)[1],
  'appId' => $config['init']['appId'],
  'appSecret' => $config['appSecret']
)));

