Hull.component({
  templates: ['main'],
  datasources: {
    user_reports: function() {
      var _ = this.sandbox.util._;
      var terms = this.options.terms.split(',');
      var params = {
        limit: 1,
        facets: {}
      };

      _.map(terms, function(term) {
        params.facets[term] = {
          terms: {
            field: term + ".exact"
          }
        };
      })

      var q = {
        path: 'search/user_reports'
      };

      if (this.options.namespace) {
        q.provider = "admin@" + this.options.namespace;
      }

      console.warn("DS: ", q, params);

      return this.api.post(q, params);
    }
  },
  initialize: function() {

  }
});
